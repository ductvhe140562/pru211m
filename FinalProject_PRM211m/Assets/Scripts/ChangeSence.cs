using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSence : MonoBehaviour
{
    public string senceName="Play";

    public GameObject musicBackGround; 
    public void LoadSence()
    {
        if (musicBackGround.active)
        {
            DontDestroyOnLoad(musicBackGround);
        }
        SceneManager.LoadScene(senceName);
    }   
}
